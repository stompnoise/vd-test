class CreateKeyvalues < ActiveRecord::Migration
  def change
    create_table :keyvalues do |t|
      
      t.string :key
      t.string :value

      t.timestamps null: false
      
    end
  end
end
