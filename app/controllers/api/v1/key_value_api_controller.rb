module Api
  module V1

    class KeyValueApiController < BaseApiController

      respond_to :json

      def create
        
        key = params.keys[0]
        value = params[key]
        
        if key
          key_value = Keyvalue.new
          key_value.key = key
          key_value.value = value
          if key_value.save
            render json: key_value
          else
            render nothing: true, status: :bad_request
          end
        else
          render nothing: true, status: :bad_request
        end
        
      end
      
      def find
        
        begin
          
          timestamp = params[:timestamp]
          
          if(timestamp)
            updated = Time.at(timestamp.to_i)
            render json: Keyvalue.where("key = ? AND updated_at >= ?", params[:id], updated).first
          else
            key_value = Keyvalue.where(key: params[:id]).last
            render :text => key_value.value
          end
          
        rescue Exception => e  
          
          render json: {error: e.message}
          
        end
               
      end

    end
  end
end